const express = require('express');
const mongoose = require('mongoose');

const app = express();
const port = 3000;

//middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));

//db connection
mongoose.set('strictQuery', true); //for deprecation warnings
mongoose.connect('mongodb+srv://zuitt-bootcamp:password12345@zuitt-bootcamp.vgg4yb3.mongodb.net/activity-35?retryWrites=true&w=majority', {
    useNewUrlParser : true,
	useUnifiedTopology : true
});


let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));
db.on("open", () => console.log("We're connected to the Mongo Atlas Cloud Database"));

const userSchema = new mongoose.Schema({
    username: String,
    password: String
});

const User = mongoose.model('User', userSchema);

app.listen(port, () => console.log(`Server is running at  http://localhost:${port}/`))

app.post('/signup', (req, res) => {
    
    User.findOne({username: req.body.username}, (error, result) => {
        if(result != null && result.username == req.body.username){
            return response.send('User already exist!');
        }

        let newUser = new User({
            username: req.body.username,
            password: req.body.password
        });

        newUser.save((error, savedUser) => {
            if(error){
                return console.log(error);
            } else {
                return res.status(200).send(`New user registered: ${req.body.username}`)
            }
        });
    });
});


app.get("/users", (req, res) => {

	User.find({}, (err, result) => {

		// If an error occured
		if (err) {
			return console.log(err);
		} else {
			return res.status(200).json({
				users : result
			});
		}
	});
});
